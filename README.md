# Paquets et images avec OpenWRT

Ce projet montre comment utiliser OpenWRT en GitLab CI/CD pour compiler un un paquet
et de l'inclure dans une image de micrologiciel.

Il suppose que les images OCI pour le SDK et l'Imagebuilder d'OpenWRT sont mise à
disposition localement dans le _container registry_ du projet frère
[midi-bidouille/openshift](https://gitlab.inria.fr/midi-bidouille/openshift).

## TL;DR

Forker ce projet, activer si besoin le CI/CD, lancer le pipeline.

Installer les paquets sur un routeur OpenWRT, ou flasher l'image de micrologiciel
directement sur le support matériel.

## _Custom feed_

L'étape `build packages` de [.gitlab-ci.yml](.gitlab-ci.yml)
créé un _custom feed_ avec des paquets précisés dans la variable `CUSTOM_PACKAGES`,
résout les dépendences puis compile le ou les paquets pour les architectures cibles
choisies. Les paquets sont mise à disposition comme artefacts de pipeline.

Ces paquets peuvent être installés sur un système OpenWRT de même architecture.

## Micrologiciel

L'étape `build packages` de [.gitlab-ci.yml](.gitlab-ci.yml)
créé une _image de de micrologiciel_ comportant un système OpenWRT de base, plus les
paquets précisés dans `CUSTOM_PACKAGES`, complétés éventuellement par un choix de
paquets standards précisé dans `BASE_PACKAGES`. Les images de micrologiel sont mise
à disposition comme artefacts de pipeline

Cette image peut être flashé sur le support matériel.
